package AngajatiApp.repository;

import AngajatiApp.model.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class EmployeeImplTest {

    private EmployeeImpl angajat;

    @Before
    public void setUp() throws Exception {
        angajat = new EmployeeImpl();
    }

    @After
    public void tearDown() throws Exception {
        angajat = null;
    }

    @Test
    public void Test1() {
        Employee c = new Employee();
        c.setId(12);
        c.setFirstName("Mar");
        c.setLastName("Moldovan");
        c.setCnp("2960321297280");
        c.setFunction(DidacticFunction.TEACHER);
        c.setSalary(2200.0);
        assertEquals(true, angajat.addEmployee(c));

    }

    @Test
    public void Test2() {
        Employee c = new Employee();
        c.setId(13);
        c.setFirstName("M");
        c.setLastName("Moldovan");
        c.setCnp("2960321297281");
        c.setFunction(DidacticFunction.TEACHER);
        c.setSalary(2200.0);
        angajat.addEmployee(c);
        assertEquals(false, angajat.addEmployee(c));
    }

    @Test
    public void Test3() {
        Employee c = new Employee();
        c.setFirstName("Maria");
        c.setLastName("Moldovan");
        c.setCnp("2960321297280");
        c.setFunction(DidacticFunction.TEACHER);
        c.setSalary(-2200.0);
        angajat.addEmployee(c);
        assertEquals(false, angajat.addEmployee(c));
    }

    @Test
    public void Test4() {
        Employee c = new Employee();
        c.setFirstName("Maria");
        c.setLastName("Moldovan");
        c.setCnp("2960321297280");
        c.setFunction(DidacticFunction.TEACHER);
        c.setSalary(100.0);
        angajat.addEmployee(c);
        assertEquals(true, angajat.addEmployee(c));
    }

    @Test
    public void Test5() {
        Employee c = new Employee();
        c.setFirstName("Maria");
        c.setLastName("Moldovan");
        c.setCnp("2960321297280");
        c.setFunction(DidacticFunction.TEACHER);
        c.setSalary(2200.0);
        angajat.addEmployee(c);
        assertEquals(true, angajat.addEmployee(c));
    }

    @Test
    public void Test6() {
        Employee c = new Employee();
        c.setFirstName("Maria");
        c.setLastName("Moldovan");
        c.setCnp("296032129720");
        c.setFunction(DidacticFunction.TEACHER);
        c.setSalary(2200.0);
        angajat.addEmployee(c);
        assertEquals(false, angajat.addEmployee(c));
    }



}